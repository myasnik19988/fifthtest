//
//  NetworkListResponse.swift
//  FifthTest
//
//  Created by Myasnik Tadevosyan on 7/4/19.
//  Copyright © 2019 MyasnikTadevosyan. All rights reserved.
//

import Foundation

struct NetworkListResponse<T> where T: Codable {
    var list: [T]
    var totalPages: Int
    var total: Int
    var page:Int
    var hasNext:Bool {return page < totalPages}
    
    
    init(list: [T] = [], totalPages: Int = 0, total: Int = 0, page:Int = 1) {
        self.list = list
        self.totalPages = totalPages
        self.total = total
        self.page = page
    }
}
