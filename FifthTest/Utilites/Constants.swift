//
//  Constants.swift
//  FifthTest
//
//  Created by Myasnik Tadevosyan on 7/4/19.
//  Copyright © 2019 MyasnikTadevosyan. All rights reserved.
//

import UIKit

final class Constants {
    
    //MARK: Base url
    
    static let baseUrl:String = "https://reqres.in/"
    
    //MARK: URL Endpoints
    
    static let usersURL:String = "api/users"
    //MARK: Requests errors
    
    static let kNotInternetConectionError:String = "not_internet_connection"
    
    static let imageTransition = UIImageView.ImageTransition.flipFromRight(1)
    
    static let notInternetConnectionAlertTitle:String = "No internet connection"
    static let notInternetConnectionAlertDesc:String = "Check your internet connection"
}
