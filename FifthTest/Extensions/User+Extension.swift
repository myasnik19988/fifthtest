//
//  User+Extension.swift
//  FifthTest
//
//  Created by Myasnik Tadevosyan on 7/4/19.
//  Copyright © 2019 MyasnikTadevosyan. All rights reserved.
//

import UIKit

extension User {
    static var defoultAvartar = UIImage(named: "defaultAvatar")!
    
    var fullName:String {return "\(first_name) \(last_name)".trimSpaces() }
}
