//
//  String+Extensions.swift
//  FifthTest
//
//  Created by Myasnik Tadevosyan on 7/5/19.
//  Copyright © 2019 MyasnikTadevosyan. All rights reserved.
//

import Foundation

extension String {
    
    func trimSpaces() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
}

