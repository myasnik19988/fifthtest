//
//  Storyboards.swift
//  FifthTest
//
//  Created by Myasnik Tadevosyan on 7/5/19.
//  Copyright © 2019 MyasnikTadevosyan. All rights reserved.
//

import UIKit

extension UIViewController {
    static func instantiate(from storyboard:Storyboards, identifier: String? = nil) -> Self {
        return instantiateFromStoryboard(storyboard.rawValue, identifier)
    }
}

enum Storyboards: String {
    
    case main = "Main"
    
    func instantiateInitial() -> UIViewController? {
        return UIStoryboard(name: self.rawValue, bundle: nil).instantiateInitialViewController()
    }
    
    func instantiateInitialNC() -> UINavigationController? {
        return UIStoryboard(name: self.rawValue, bundle: nil).instantiateInitialViewController() as? UINavigationController
    }
    
    func instantiate(identifier: String) -> UIViewController {
        return UIStoryboard(name: self.rawValue, bundle: nil).instantiateViewController(withIdentifier: identifier)
    }
    
    func instantiate<T:UIViewController> (identifier: String, type: T.Type) -> T {
        return UIStoryboard(name: self.rawValue, bundle: nil).instantiateViewController(withIdentifier: identifier) as! T
    }
    
}
