//
//  UIViewController+Extension.swift
//  FifthTest
//
//  Created by Myasnik Tadevosyan on 7/5/19.
//  Copyright © 2019 MyasnikTadevosyan. All rights reserved.
//

import UIKit

extension UIViewController {
    
    static func instantiateFromStoryboard(_ name: String = "Main", _ identifier: String? = nil) -> Self {
        func instantiateFromStoryboardHelper<T>(_ name: String) -> T {
            let storyboard = UIStoryboard(name: name, bundle: nil)
            let id = identifier ?? String(describing: self)
            let controller = storyboard.instantiateViewController(withIdentifier: id) as! T
            return controller
        }
        return instantiateFromStoryboardHelper(name)
    }
    
    func getViewControllerBy(identifire:String, storyboardName:String) -> UIViewController {
        let vc = UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: identifire)
        return vc
    }
    
    static func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        
        return base
    }
    
}
