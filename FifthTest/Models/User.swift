//
//  User.swift
//  FifthTest
//
//  Created by Myasnik Tadevosyan on 7/4/19.
//  Copyright © 2019 MyasnikTadevosyan. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object, Codable {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var email: String = ""
    @objc dynamic var first_name: String = ""
    @objc dynamic var last_name: String = ""
    @objc dynamic var avatar: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case email
        case first_name
        case last_name
        case avatar
    }
    
}
