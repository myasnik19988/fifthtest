//
//  UserTableViewCell.swift
//  FifthTest
//
//  Created by Myasnik Tadevosyan on 7/4/19.
//  Copyright © 2019 MyasnikTadevosyan. All rights reserved.
//

import UIKit
import AlamofireImage

class UserTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    
    var user:User! {
        didSet {
            updateUI(user: user)
        }
    }
    
    private func updateUI(user:User) {
        self.fullNameLabel.text = user.fullName
        if let url = URL(string: user.avatar) {
            self.userAvatar.af_setImage(withURL: url,
                                        placeholderImage: User.defoultAvartar,
                                        progressQueue: .global(),
                                        imageTransition: Constants.imageTransition)
        }
    }
    
}
