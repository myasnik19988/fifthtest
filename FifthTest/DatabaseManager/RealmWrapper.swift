//
//  RealmWrapper.swift
//  FifthTest
//
//  Created by Myasnik Tadevosyan on 7/4/19.
//  Copyright © 2019 MyasnikTadevosyan. All rights reserved.
//

import Foundation
import RealmSwift

let realm = try! Realm()

class RealmWrapper {
    
    func getAllObjects(of objectType: Object.Type, sortedKey: String? = nil)->[Object] {
        
        var objects = realm.objects(objectType)
        
        if let sortedKey = sortedKey {
            objects = objects.sorted(byKeyPath: sortedKey, ascending: true)
        }
        
        return Array(objects)
    }
    
    func add(contentOf objects: [Object]) {
        objects.forEach({RealmWrapper().add(object:$0)})
    }
    
    func add(object: Object) {
        do {
            try realm.write({
                realm.add(object, update: Realm.UpdatePolicy.modified)
            })
        } catch {
            debugPrint("Something went wrong!")
        }
    }
    
    func deleteAll()  {
        do {
            try realm.write({
                realm.deleteAll()
            })
        } catch {
            debugPrint("Something went wrong!")
        }
    }
    
}
