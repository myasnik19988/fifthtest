//
//  AlamofireWrapper.swift
//  FifthTest
//
//  Created by Myasnik Tadevosyan on 7/4/19.
//  Copyright © 2019 MyasnikTadevosyan. All rights reserved.
//

import Foundation
import Alamofire

final class AlamofireWrapper {
    
    static let sessionManager = Alamofire.SessionManager.default
    
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    class func getRequest(_strUrl: String, params: [String:Any]?, headers: HTTPHeaders?, success: @escaping(Any) -> Void, failure: @escaping(Any) -> Void) {
        
        if !AlamofireWrapper.isConnectedToInternet() {
            failure(Constants.kNotInternetConectionError)
            return
        }
        
        let params: Parameters? = params ?? nil
        
        sessionManager.request(Constants.baseUrl+_strUrl, method: HTTPMethod.get, parameters: params , encoding: URLEncoding.default, headers: nil).validate().responseData { (response) in
            if response.result.isSuccess, let jsonData = response.result.value {
                success(jsonData)
                return
            }
            
            if response.result.isFailure, let data = response.data {
                do {
                    if let errorData = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        failure(errorData)
                        return
                    }
                } catch {
                    debugPrint(response.error?.localizedDescription ?? "Request has error")
                }
            }
            failure("Error")
        }
    }
}
