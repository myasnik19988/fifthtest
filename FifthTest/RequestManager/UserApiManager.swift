//
//  UserApiManager.swift
//  FifthTest
//
//  Created by Myasnik Tadevosyan on 7/4/19.
//  Copyright © 2019 MyasnikTadevosyan. All rights reserved.
//

import Foundation

class UserApiManager {
    
    private var totalPages:Int = 1
    private var total:Int = 1
    private var page:Int = 1
    
    func getUsers(page: Int, perPage: Int, succses:@escaping(Any) -> Void, failure: @escaping(Any) -> Void) {
        AlamofireWrapper.getRequest(_strUrl: Constants.usersURL, params: ["page": page, "per_page": perPage], headers: nil, success: { [weak self] (response) in
            guard let `self` = self, let jsonData = response as? Data else {
                failure("Error")
                return
            }
            
            if let stringDic = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [String : Any] {
                if let page = stringDic["page"] as? Int ,
                    let totalPages = stringDic["total_pages"] as? Int ,
                    let total  = stringDic["total"] as? Int {
                    
                    self.totalPages = totalPages
                    self.page = page
                    self.total = total
                }
            }
            if let users = try? JSONDecoder().decode([User].self, from: jsonData, keyPath: "data") {
                
                let networkListResponse = NetworkListResponse(list: users,
                                                              totalPages: self.totalPages,
                                                              total: self.total,
                                                              page: self.page)
                succses(networkListResponse)
                return
            }
            failure("Error")
            
        }) { (error) in
            failure(error)
        }
    }
    
    func getSingleUserBy(id:Int, succses:@escaping(Any) -> Void, failure: @escaping(Any) -> Void) {
        AlamofireWrapper.getRequest(_strUrl: Constants.usersURL + "/\(id)", params: nil, headers: nil, success: { (response) in
            if let jsonData = response as? Data,
                let user = try? JSONDecoder().decode(User.self, from: jsonData, keyPath: "data") {
                RealmWrapper().add(object: user)
                succses(user)
                return
            }
            failure("Error")
        }) { (error) in
            failure(error)
        }
    }
}
