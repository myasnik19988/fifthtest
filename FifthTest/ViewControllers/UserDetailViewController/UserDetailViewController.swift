//
//  UserDetailViewController.swift
//  FifthTest
//
//  Created by Myasnik Tadevosyan on 7/4/19.
//  Copyright © 2019 MyasnikTadevosyan. All rights reserved.
//

import UIKit

class UserDetailViewController: UIViewController {
    
    @IBOutlet weak var userAvatarImageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    private lazy var requestManager = UserApiManager()
    private var user:User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI(user: user)
        title = user.first_name
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func setupUI(user:User) {
        self.fullNameLabel.text = user.fullName
        self.emailLabel.text = user.email
        if let url = URL(string: user.avatar) {
            self.userAvatarImageView.af_setImage(withURL: url,
                                                 placeholderImage: User.defoultAvartar,
                                                 progressQueue: .global())
        }
    }
    
    func setupPageData(user:User) {
        self.user = user
        getSingleUser(userId: user.id)
    }
    
    func getSingleUser(userId:Int) {
        requestManager.getSingleUserBy(id: userId, succses: { (response) in
            if let user = response as? User {
                self.user = user
                self.setupUI(user: user)
            }
        }, failure: { error in
            print(error)
        })
    }
    
}
