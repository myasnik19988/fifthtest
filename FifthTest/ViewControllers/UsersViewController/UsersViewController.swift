//
//  UsersViewController.swift
//  FifthTest
//
//  Created by Myasnik Tadevosyan on 7/4/19.
//  Copyright © 2019 MyasnikTadevosyan. All rights reserved.
//

import UIKit
import SVPullToRefresh

class UsersViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override var prefersStatusBarHidden: Bool {return true}
    private var users = [User]()
    lazy var requestManager = UserApiManager()
    private var page = 1
    private var userNetworkResponse:NetworkListResponse<User>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
        getUsers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func getUsers(fromStart: Bool = false) {
        if fromStart {
            page = 1
        }
        
        if page < 0 {
            self.tableView.infiniteScrollingView.stopAnimating()
            return
        }
        
        requestManager.getUsers(page: page, perPage: 6, succses: { [weak self] (response) in
            guard let `self` = self else {return}
            self.stopLoading()
            if let response = response as? NetworkListResponse<User> {
                self.page = response.hasNext ? self.page + 1 : -1
                if fromStart {
                    RealmWrapper().deleteAll()
                    self.users.removeAll()
                }
                RealmWrapper().add(contentOf: response.list)
                
                self.userNetworkResponse = response
            }
            self.users = RealmWrapper().getAllObjects(of: User.self) as! [User]
            self.tableView.reloadData()
        }) { [weak self] (error) in
            if let error = error as? String, error == Constants.kNotInternetConectionError {
                self?.showAlert(title: Constants.notInternetConnectionAlertTitle, desc: Constants.notInternetConnectionAlertDesc)
            }
            self?.stopLoading()
        }
    }
    
    func stopLoading() {
        tableView.infiniteScrollingView.stopAnimating()
        tableView.pullToRefreshView.stopAnimating()
    }
    
    func initTableView() {
        self.tableView.register(UINib(nibName: UserTableViewCell.nameOfClass,
                                      bundle: nil),
                                forCellReuseIdentifier: UserTableViewCell.nameOfClass)
        
        tableView.addInfiniteScrolling { [weak self] in
            self?.getUsers()
        }
        
        tableView.addPullToRefresh { [weak self] in
            self?.getUsers(fromStart: true)
        }
        
    }
    
    func showAlert(title: String, desc: String? = nil) {
        Alert.showAlert(title, message: desc)
    }
}

extension UsersViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UserTableViewCell.nameOfClass, for: indexPath) as! UserTableViewCell
        cell.user = users[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UserDetailViewController.instantiate(from: .main)
        vc.setupPageData(user: users[indexPath.row])
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
